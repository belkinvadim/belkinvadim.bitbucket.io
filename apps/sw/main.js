const button = document.querySelector('button');

button.innerText = 'Button';

if ('Notification' in window) {
    if (Notification.permission !== 'denied') {
        Notification.requestPermission();
    }

    button.addEventListener('click', () => {
        navigator.serviceWorker.ready.then((registration) => {
            registration.showNotification('Hi!', {
                body: 'body',
                icon: 'http://www.technoburgh.com/wp-content/uploads/2015/01/android-logo.jpg',
                image: 'https://peter-gribanov.github.io/serviceworker/Bubble-Nebula_big.jpg',
            });
        });
    });
}
