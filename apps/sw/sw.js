const CACHE_DATA = {
    name: 'app',
    resources: [
        './index.html',
        './main.css',
        './main.js',
    ],
};

self.addEventListener('install', (event) => {
    console.log('Install');
    event.waitUntil(
        caches.open(CACHE_DATA.name).then(cache => cache.addAll(CACHE_DATA.resources))
    );
});

self.addEventListener('activate', (event) => {
    console.log('Activate');
    event.waitUntil(
        // Получение всех ключей из кэша.
        caches.keys()
            .then((cacheNames) => {
                return Promise.all(
                    // Прохождение по всем кэшам
                    cacheNames.map((cacheName) => {
                        // Если имя кэша отличается от заданного имени,
                        // данный кэш следует удалить
                        if (cacheName !== CACHE_DATA.name) {
                            console.log(`Delete cache ${cacheName}`);
                            return caches.delete(cacheName);
                        }
                    })
                );
            })
    );
});

self.addEventListener('fetch', (event) => {
    console.log('Fetch', event.request.url);
    event.respondWith(
        // Этот метод анализирует запрос и
        // ищет кэшированные результаты для этого запроса в любом из
        // созданных сервис-воркером кэшей.
        caches.match(event.request)
            .then((response, response2) => {
                console.log(response, response2);
                // если в кэше найдено то, что нужно, мы можем тут же вернуть ответ.
                if (response) {
                    console.log('кеш найден');
                    return response;
                }

                // IMPORTANT: Клонируем запрос. Так как объект запроса - это поток,
                // обратиться к нему можно лишь один раз.
                // При этом один раз мы обрабатываем его для нужд кэширования,
                // ещё один раз он обрабатывается браузером, для запроса ресурсов,
                // поэтому объект запроса нужно клонировать.
                const fetchRequest = event.request.clone();

                // В кэше ничего не нашлось, поэтому нужно выполнить загрузку материалов,
                // что заключается в выполнении сетевого запроса и в возврате данных, если
                // то, что нужно, может быть получено из сети.
                return fetch(fetchRequest)
                    .then((response) => {
                        // Проверка того, получили ли мы правильный ответ
                        if(!response || response.status !== 200 || response.type !== 'basic') {
                            return response;
                        }

                        // IMPORTANT: Клонирование объекта ответа, так как он тоже является потоком.
                        // Так как нам надо, чтобы ответ был обработан браузером,
                        // а так же кэшем, его нужно клонировать,
                        // поэтому в итоге у нас будет два потока.
                        const responseToCache = response.clone();

                        caches.open(CACHE_DATA.name)
                            .then((cache) => {
                                // Добавляем ответ в кэш для последующего использования.
                                console.log('добавление в кеш');
                                cache.put(event.request, responseToCache);
                            });

                        return response;
                    });
            })
    );
});
