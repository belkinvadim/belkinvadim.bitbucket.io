import '../base-elements/base-list-item.js';

const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      position: relative;
      box-sizing: border-box;
      display: block;
    }

    .container {
      display: grid;
      grid-template-areas: "icon body";
      grid-template-columns: max-content auto;
      grid-column-gap: 16px;
      align-items: center;
    }

    .icon {
      display: grid;
      align-items: center;
      justify-content: center;
      grid-area: icon;
      width: 40px;
      height: 40px;
      background-color: rgba(0, 0, 0, .24);
      border-radius: 50%;
    }

    .body {
      grid-area: body;
    }
  </style>

  <base-list-item class="container">
    <div class="icon">
      <slot name="icon"></slot>
    </div>

    <div class="body">
      <slot></slot>
    </div>
  </base-list-item>
`;

class AppTransactionListItem extends HTMLElement {
  static get observedAttributes() {
    return ['heading'];
  }

  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define('app-transaction-list-item', AppTransactionListItem);
