class BehaviorButton extends HTMLElement {
  static get observedAttributes() {
    return ['disabled'];
  }

  set disabled(value) {
    const isDisabled = Boolean(value);

    if (isDisabled) {
      this.setAttribute('disabled', '');
    } else {
      this.removeAttribute('disabled');
    }
  }

  get disabled() {
    return this.hasAttribute('disabled');
  }

  constructor() {
    super();

    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
  }

  connectedCallback() {
    if (!this.hasAttribute('role')) {
      this.setAttribute('role', 'button');
    }

    if (!this.hasAttribute('tabindex')) {
      this.setAttribute('tabindex', 0);
    }

    this.addEventListener('keydown', this.handleKeyDown);
    this.addEventListener('keyup', this.handleKeyUp);
  }

  disconnectedCallback() {
    this.removeEventListener('keydown', this.handleKeyDown);
    this.removeEventListener('keyup', this.handleKeyUp);
  }

  attributeChangedCallback(attributeName) {
    if (attributeName === 'disabled') {
      this.setAttribute('aria-disabled', this.disabled);
      this.setAttribute('tabindex', this.disabled ? -1 : 0);
    }
  }

  dispatchClick() {
    const eventClick = new Event('click');
    this.dispatchEvent(eventClick);
  }

  handleKeyDown(event) {
    if (event.key === 'Enter' || event.key === ' ') {
      event.preventDefault();
    }

    if (event.key === 'Enter') {
      this.dispatchClick();
    }
  }

  handleKeyUp(event) {
    if (event.key === ' ') {
      event.preventDefault();
      this.dispatchClick();
    }
  }
}

export default BehaviorButton;
