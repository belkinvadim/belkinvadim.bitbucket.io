class _behaviorMenu extends HTMLElement {
  constructor() {
    super();

    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleTargetClick = this.handleTargetClick.bind(this);
  }

  connectedCallback() {
    if (!this.hasAttribute('role')) {
      this.setAttribute('role', 'menu');
    }

    if (!this.hasAttribute('tabindex')) {
      this.setAttribute('tabindex', -1);
    }

    this.hide();

    this.target = document.querySelector(`#${this.getAttribute('target')}`);

    if (!this.target) {
      return;
    }

    this.addEventListener('keydown', this.handleKeyDown);
    this.target.addEventListener('click', this.handleTargetClick);
  }

  disconnectedCallback() {
    if (!this.target) {
      return;
    }

    this.removeEventListener('keydown', this.handleKeyDown);
    this.target.removeEventListener('click', this.handleTargetClick);

    this.target = null;
  }

  dispatchHide() {
    const eventHide = new Event('hide');
    this.dispatchEvent(eventHide);
  }

  handleKeyDown(event) {
    if (event.key === 'Escape') {
      event.preventDefault();
      this.hide();
    }
  }

  handleTargetClick() {}

  show() {
    this.prevActiveElement = document.activeElement;
    this.hidden = false;
    this.setAttribute('tabindex', 0);

    const menuRect = this.getBoundingClientRect();
    const targetRect = this.target.getBoundingClientRect();

    const targetCenter = targetRect.left + targetRect.width / 2;
    const centerOffset = targetCenter - menuRect.width / 2;
    const maxOffset = window.innerWidth - menuRect.width;

    this.style.top = `${targetRect.bottom}px`;
    this.style.left = `${Math.max(0, Math.min(maxOffset, centerOffset))}px`;

    this.focus();
  }

  hide() {
    this.hidden = true;

    this.style.top = '';
    this.style.left = '';
    this.setAttribute('tabindex', -1);

    this.dispatchHide();

    if (this.prevActiveElement) {
      this.prevActiveElement.focus();
    }
  }
}

export default _behaviorMenu;
