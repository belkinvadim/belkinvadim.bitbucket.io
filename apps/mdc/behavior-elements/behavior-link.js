class BehaviorLink extends HTMLElement {
  static get observedAttributes() {
    return ['href'];
  }

  set href(value) {
    this.setAttribute('href', value);
  }

  get href() {
    return this.getAttribute('href');
  }

  constructor() {
    super();

    this.handleClick = this.handleClick.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }

  connectedCallback() {
    if (!this.hasAttribute('role')) {
      this.setAttribute('role', 'link');
    }

    if (!this.hasAttribute('tabindex')) {
      this.setAttribute('tabindex', 0);
    }

    this.addEventListener('click', this.handleClick);
    this.addEventListener('keydown', this.handleKeyDown);
  }

  disconnectedCallback() {
    this.removeEventListener('click', this.handleClick);
    this.removeEventListener('keydown', this.handleKeyDown);
  }

  attributeChangedCallback(attributeName) {
    if (attributeName === 'disabled') {
      this.setAttribute('aria-disabled', this.disabled);
      this.setAttribute('tabindex', this.disabled ? -1 : 0);
    }
  }

  goToLink() {
    if (this.href ===  null) {
      return;
    }

    document.location.href = this.href;
  }

  dispatchClick() {
    const eventClick = new Event('click');
    this.dispatchEvent(eventClick);
  }

  handleClick() {
    this.goToLink();
  }

  handleKeyDown(event) {
    if (event.key === 'Enter') {
      event.preventDefault();
      this.dispatchClick();
      this.goToLink();
    }
  }
}

export default BehaviorLink;
