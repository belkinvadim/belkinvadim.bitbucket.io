const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 160;
    }

    :host(:not([open])) {
      pointer-events: none;
    }

    .backdrop {
      position: absolute;
      top: 0;
      left: 0;
      z-index: 1;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, .32);
      opacity: 1;
      cursor: pointer;
      transition: opacity 250ms var(--transition-standart-easing);
      will-change: opacity;
    }

    :host(:not([open])) .backdrop {
      opacity: 0;
      transition-duration: 200ms;
    }

    .container {
      position: relative;
      z-index: 2;
      width: 100%;
      max-width: 280px;
      height: 100%;
      overflow: auto;
      color: var(--color-on-surface);
      background-color: var(--color-surface);
      box-shadow: var(--shadow-elevation-16);
      transform: translateX(0);
      transition: transform 250ms var(--transition-standart-easing);
      will-change: transform;
    }

    :host(:not([open])) .container {
      box-shadow: none;
      transform: translateX(-100%);
      transition-duration: 200ms;
    }

    ::slotted(hr) {
      width: 100%;
      height: 0;
      margin: 4px 0;
      border: none;
      border-bottom: 1px solid var(--color-divider-on-background);
    }
  </style>

  <div class="backdrop"></div>

  <div class="container">
    <slot />
  </div>
`;

class BaseDrawer extends HTMLElement {
  static get observedAttributes() {
    return ['open'];
  }

  set open(value) {
    const isOpen = Boolean(value);

    if (isOpen) {
      this.setAttribute('open', '');
    } else {
      this.removeAttribute('open');
    }
  }

  get open() {
    return this.hasAttribute('open');
  }

  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));

    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleBackdropClick = this.handleBackdropClick.bind(this);
  }

  connectedCallback() {
    this.addEventListener('keydown', this.handleKeyDown);
    this.shadowRoot
      .querySelector('.backdrop')
      .addEventListener('click', this.handleBackdropClick);
  }

  disconnectedCallback() {
    this.removeEventListener('keydown', this.handleKeyDown);
    this.shadowRoot
      .querySelector('.backdrop')
      .removeEventListener('click', this.handleBackdropClick);
  }

  dispatchClose() {
    const eventClose = new Event('close');
    this.dispatchEvent(eventClose);
  }

  close() {
    this.open = false;
    this.dispatchClose();
  }

  handleKeyDown(event) {
    if (event.key === 'Escape') {
      event.preventDefault();
      this.close();
    }
  }

  handleBackdropClick() {
    this.close();
  }
}

customElements.define('base-drawer', BaseDrawer);
