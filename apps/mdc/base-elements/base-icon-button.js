import BehaviorButton from '../behavior-elements/behavior-button.js';

const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      position: relative;
      box-sizing: border-box;
      display: inline-block;
      width: 48px;
      height: 48px;
      padding: 12px;
      line-height: 24px;
      color: var(--color-text-primary-on-background);
      background-color: transparent;
      user-select: none;
      cursor: pointer;
    }

    :host(:focus) {
      outline: none;
    }

    :host([hidden]) {
      display: none;
    }

    :host([disabled]) {
      color: var(--color-text-disabled-on-background);
      pointer-events: none;
      cursor: auto;
    }

    :host::before,
    :host::after {
      content: "";
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      background-color: var(--color-on-background);
      border-radius: 50%;
      opacity: 0;
      pointer-events: none;
    }

    :host::before {
      transition: opacity 75ms var(--transition-standart-easing);
    }

    :host(:hover)::before {
      opacity: .04;
    }

    :host(:focus)::before {
      opacity: .12;
    }

    :host::after {
      transform:
        scale(0)
        translateZ(-1px);
      transform-origin: center center;
      transition:
        opacity .15s var(--transition-standart-easing),
        transform .15s var(--transition-standart-easing) .15s;
    }

    :host(:active)::after {
      opacity: .12;
      transform:
        scale(1)
        translateZ(-1px);
      transition-duration: .225s;
      transition-delay: 0s;
    }
  </style>

  <slot></slot>
`;

class BaseIconButton extends BehaviorButton {
  constructor() {
    super();

    this.attachShadow({mode: 'open'});
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define('base-icon-button', BaseIconButton);
