import BehaviorButton from '../behavior-elements/behavior-button.js';

const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      position: relative;
      box-sizing: border-box;
      display: inline-grid;
      align-content: center;
      min-width: 64px;
      height: 36px;
      margin: 0;
      padding: 0 8px;
      font-size: 14px;
      font-weight: 500;
      letter-spacing: .75px;
      line-height: 24px;
      text-align: center;
      text-transform: uppercase;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
      color: var(--color-primary);
      background-color: transparent;
      border-radius: 4px;
      user-select: none;
      outline: none;
      cursor: pointer;
    }

    :host([hidden]) {
      display: none;
    }

    :host([disabled]) {
      color: var(--color-text-disabled-on-background);
      pointer-events: none;
      cursor: auto;
    }

    :host::before,
    :host::after {
      content: "";
      position: absolute;
      background-color: var(--color-primary);
      opacity: 0;
      pointer-events: none;
    }

    :host::before {
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      transition: opacity 75ms var(--transition-standart-easing);
    }

    :host(:hover)::before {
      opacity: .04;
    }

    :host(:focus)::before {
      opacity: .12;
    }

    :host::after {
      top: 50%;
      left: 50%;
      width: 110%;
      padding-bottom: 110%;
      border-radius: 50%;
      transform:
        translate(-50%, -50%)
        translateZ(-1px)
        scale(0);
      transform-origin: center center;
      transition:
        opacity .15s var(--transition-standart-easing),
        transform .15s var(--transition-standart-easing) .15s;
    }

    :host(:active)::after {
      opacity: .24;
      transform:
        translate(-50%, -50%)
        translateZ(-1px)
        scale(1);
      transition-duration: .225s;
      transition-delay: 0s;
    }

    :host([type="outlined"]) {
      padding-right: 15px;
      padding-left: 15px;
      border: 1px solid var(--color-primary);
    }

    :host([type="outlined"][disabled]) {
      color: var(--color-text-disabled-on-background);
      border-color: var(--color-text-disabled-on-background);
    }

    :host([type="outlined"])::after {
      background-color: var(--color-on-primary);
    }

    :host([type="contained"]) {
      padding-right: 16px;
      padding-left: 16px;
      color: var(--color-on-primary);
      background-color: var(--color-primary);
    }

    :host([type="contained"][disabled]) {
      color: var(--color-text-disabled-on-background);
      background-color: var(--color-divider-on-background);
    }

    :host([type="contained"])::before {
      background-color: var(--color-on-primary);
    }

    :host([type="contained"]:hover)::before {
      opacity: .08;
    }

    :host([type="contained"]:focus)::before {
      opacity: .24;
    }

    :host([type="contained"]:active)::after {
      opacity: .24;
    }
  </style>

  <slot></slot>
`;

class BaseButton extends BehaviorButton {
  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define('base-button', BaseButton);
