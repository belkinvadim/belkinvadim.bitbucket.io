const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      position: fixed;
      top: 0;
      left: 0;
      z-index: 1000;
      box-sizing: border-box;
      display: block;
      max-width: 100vw;
      min-height: 32px;
      margin-top: 4px;
      padding: 8px 16px;
      font-size: 12px;
      font-weight: 500;
      line-height: 16px;
      letter-spacing: .8px;
      color: #fff;
      background-color: rgba(60, 64, 67, 0.9);
      border-radius: 4px;
      pointer-events: none;
    }

    :host([hidden]) {
      display: none;
    }

    @media (min-width: 720px) {
      :host {
        min-height: 24px;
        padding: 4px 8px;
      }
    }
  </style>

  <slot></slot>
`;

class BaseTooltip extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));

    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
  }

  connectedCallback() {
    if (!this.hasAttribute('role')) {
      this.setAttribute('role', 'tooltip');
    }

    if (!this.hasAttribute('tabindex')) {
      this.setAttribute('tabindex', -1);
    }

    this.hide();

    this.target = document.querySelector('[aria-describedby=' + this.id + ']');

    if (!this.target) {
      return;
    }

    this.target.addEventListener('focus', this.show);
    this.target.addEventListener('blur', this.hide);
    this.target.addEventListener('mouseenter', this.show);
    this.target.addEventListener('mouseleave', this.hide);
  }

  disconnectedCallback() {
    if (!this.target) {
      return;
    }

    this.target.removeEventListener('focus', this.show);
    this.target.removeEventListener('blur', this.hide);
    this.target.removeEventListener('mouseenter', this.show);
    this.target.removeEventListener('mouseleave', this.hide);

    this.target = null;
  }

  show() {
    this.hidden = false;

    const tooltipRect = this.getBoundingClientRect();
    const targetRect = this.target.getBoundingClientRect();

    const targetCenter = targetRect.left + targetRect.width / 2;
    const centerOffset = targetCenter - tooltipRect.width / 2;
    const maxOffset = window.innerWidth - tooltipRect.width;

    this.style.top = `${targetRect.bottom}px`;
    this.style.left = `${Math.max(0, Math.min(maxOffset, centerOffset))}px`;
  }

  hide() {
    this.hidden = true;

    this.style.top = '';
    this.style.left = '';
  }
}

customElements.define('base-tooltip', BaseTooltip);
