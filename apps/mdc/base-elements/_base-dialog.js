const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      position: fixed;
      top: 0;
      left: 0;
      z-index: 24;
      box-sizing: border-box;
      display: grid;
      width: 100%;
      height: 100%;
      padding: 16px;
      overflow: hidden;
      grid-template-rows: minmax(0px, max-content);
      align-items: center;
      align-content: center;
      justify-items: center;
    }

    :host(:not([open])) {
      pointer-events: none;
    }

    .backdrop {
      position: fixed;
      top: 0;
      left: 0;
      z-index: 1;
      display: block;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, .32);
      cursor: pointer;
    }

    :host(:not([open])) .backdrop {
      opacity: 0;
    }

    .container {
      position: relative;
      z-index: 2;
      display: grid;
      width: 100%;
      max-width: 500px;
      height: 100%;
      grid-template-areas:
        "heading"
        "content"
        "actions";
      grid-template-rows: max-content auto max-content;
      color: var(--color-on-surface);
      background-color: var(--color-surface);
      border-radius: 4px;
      box-shadow: var(--shadow-elevation-24);
    }

    :host(:not([open])) .container {
      display: none;
    }

    .content {
      grid-area: content;
      max-height: 100%;
      padding: var(--margin);
      overflow: auto;
      color: var(--color-text-secondary-on-background);
    }

    .actions {
      display: grid;
      grid-area: actions;
      grid-gap: 12px;
      grid-auto-flow: column;
      grid-auto-columns: max-content;
      justify-content: end;
      padding: 10px 12px;
    }
  </style>

  <div class="backdrop"></div>

  <div class="container">
    <div class="content">
      <slot />
    </div>

    <div class="actions">
      <slot name="actions" />
    </div>
  </div>
`;

class _baseDialog extends HTMLElement {
  static get observedAttributes() {
    return ['open'];
  }

  set open(value) {
    const isOpen = Boolean(value);

    if (isOpen) {
      this.setAttribute('open', '');
    } else {
      this.removeAttribute('open');
    }
  }

  get open() {
    return this.hasAttribute('open');
  }

  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));

    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleBackdropClick = this.handleBackdropClick.bind(this);
  }

  connectedCallback() {
    if (!this.hasAttribute('role')) {
      this.setAttribute('role', 'dialog');
    }

    this.addEventListener('keydown', this.handleKeyDown);
    this.shadowRoot
      .querySelector('.backdrop')
      .addEventListener('click', this.handleBackdropClick);
  }

  disconnectedCallback() {
    this.removeEventListener('keydown', this.handleKeyDown);
    this.shadowRoot
      .querySelector('.backdrop')
      .removeEventListener('click', this.handleBackdropClick);
  }

  dispatchClose() {
    const eventClose = new Event('close');
    this.dispatchEvent(eventClose);
  }

  close() {
    this.open = false;
    this.dispatchClose();
  }

  handleKeyDown(event) {
    if (event.key === 'Escape') {
      event.preventDefault();
      this.close();
    }
  }

  handleBackdropClick() {
    this.close();
  }
}

customElements.define('base-dialog', _baseDialog);
