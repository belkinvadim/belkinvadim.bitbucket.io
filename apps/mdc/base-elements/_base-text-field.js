const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      position: relative;
      box-sizing: border-box;
      display: block;
    }

    :host(:focus) {
      outline: none;
    }

    :host([disabled]) {
      pointer-events: none;
    }

    :host([hidden]) {
      display: none;
    }

    .field {
      box-sizing: border-box;
      overflow: hidden;
      display: block;
      width: 100%;
      min-height: 56px;
      padding: 13px 15px;
      font-family: inherit;
      font-size: 16px;
      font-weight: 400;
      letter-spacing: .15px;
      line-height: 28px;
      text-transform: none;
      white-space: nowrap;
      color: var(--color-text-primary-on-background);
      background-color: var(--color-background);
      border: 1px solid var(--color-divider-on-background);
      border-radius: 4px;
      transition:
        border-color .15s var(--transition-standart-easing),
        box-shadow .15s var(--transition-standart-easing);
    }

    :host(:hover) .field {
      border-color: var(--color-text-primary-on-background);
    }

    :host(:focus-within) .field {
      border-color: var(--color-primary);
      box-shadow: inset 0 0 0 1px var(--color-primary);
      outline: none;
    }

    :host([disabled]) .field {
      color: var(--color-text-secondary-on-background);
      border-color: rgba(0, 0, 0, .06);
    }

    :host([multiline]) .field {
      overflow: auto;
      min-height: 84px;
      max-height: 140px;
      white-space: normal;
    }

    .label {
      position: absolute;
      top: 0;
      left: 16px;
      z-index: 2;
      display: block;
      max-width: calc(100% - 32px);
      font-size: 16px;
      font-weight: 400;
      letter-spacing: .15px;
      line-height: 1;
      text-transform: none;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
      color: var(--color-text-secondary-on-background);
      background-color: rgba(255, 255, 255, 0);
      transform: translateY(20px);
      transform-origin: left top;
      transition:
        top .15s var(--transition-standart-easing),
        color .15s var(--transition-standart-easing),
        background-color .15s var(--transition-standart-easing),
        transform .15s var(--transition-standart-easing);
      pointer-events: none;
    }

    .label:empty {
      display: none;
    }

    .field:not(:empty) + .label,
    :host(:focus-within) .label {
      color: var(--color-primary);
      background-color: var(--color-background);
      box-shadow: 0 0 0 4px var(--color-background);
      transform: scale(.75) translateY(-50%);
    }

    :host([disabled]) .label{
      color: var(--color-text-disabled-on-background);
    }
  </style>

  <div
    class="field"
    contenteditable="true"
    inputmode="text"
  ></div>

  <div
    id="label"
    class="label"
  ></div>
`;

class BaseTextField extends HTMLElement {
  static get observedAttributes() {
    return ['label', 'type', 'multiline', 'readonly', 'disabled', 'value'];
  }

  set label(value) {
    this.setAttribute('label', value);
  }

  get label() {
    return this.getAttribute('label');
  }

  set type(value) {
    this.setAttribute('type', value);
  }

  get type() {
    return this.getAttribute('type');
  }

  set multiline(value) {
    const isMultiline = Boolean(value);

    if (isMultiline) {
      this.setAttribute('multiline', '');
    } else {
      this.removeAttribute('multiline');
    }
  }

  get multiline() {
    return this.hasAttribute('multiline');
  }

  set readonly(value) {
    const isReadonly = Boolean(value);

    if (isReadonly) {
      this.setAttribute('readonly', '');
    } else {
      this.removeAttribute('readonly');
    }
  }

  get readonly() {
    return this.hasAttribute('readonly');
  }

  set disabled(value) {
    const isDisabled = Boolean(value);

    if (isDisabled) {
      this.setAttribute('disabled', '');
    } else {
      this.removeAttribute('disabled');
    }
  }

  get disabled() {
    return this.hasAttribute('disabled');
  }

  set value(value) {
    this.shadowRoot.querySelector('.field').innerText = value;
  }

  get value() {
    return this.shadowRoot.querySelector('.field').innerText;
  }

  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));

    this.handleFieldInput = this.handleFieldInput.bind(this);
    this.handleFieldKeyDown = this.handleFieldKeyDown.bind(this);
    this.handleFieldPaste = this.handleFieldPaste.bind(this);
  }

  connectedCallback() {
    if (!this.hasAttribute('role')) {
      this.setAttribute('role', 'textbox');
    }

    if (this.hasAttribute('value')) {
      this.value = this.getAttribute('value');
    }

    this.shadowRoot
      .querySelector('.field')
      .addEventListener('input', this.handleFieldInput);
    this.shadowRoot
      .querySelector('.field')
      .addEventListener('keydown', this.handleFieldKeyDown);
    this.shadowRoot
      .querySelector('.field')
      .addEventListener('paste', this.handleFieldPaste);
  }

  disconnectedCallback() {
    this.shadowRoot
      .querySelector('.field')
      .removeEventListener('input', this.handleFieldInput);
    this.shadowRoot
      .querySelector('.field')
      .removeEventListener('keydown', this.handleFieldKeyDown);
    this.shadowRoot
      .querySelector('.field')
      .removeEventListener('paste', this.handleFieldPaste);
  }

  attributeChangedCallback(attributeName, oldValue, newValue) {
    switch(attributeName) {
      case 'label':
        this.setAttribute('aria-label', newValue);
        this.shadowRoot.querySelector('.label').textContent = newValue;
        break;
      case 'type':
        this.shadowRoot
          .querySelector('.field')
          .setAttribute('inputmode', newValue);
        break;
      case 'multiline':
        this.setAttribute('aria-multiline', this.multiline);
        break;
      case 'readonly':
        this.setAttribute('aria-readonly', this.readonly);
        this.shadowRoot
          .querySelector('.field')
          .setAttribute('contenteditable', !this.readonly);
        break;
      case 'disabled':
        this.setAttribute('aria-disabled', this.disabled);
        this.setAttribute('tabindex', this.disabled ? -1 : 0);
        this.shadowRoot
          .querySelector('.field')
          .setAttribute('contenteditable', !this.disabled);
        break;
      case 'value':
        this.value = newValue;
        break;
      default:
        break;
    }
  }

  handleFieldInput(event) {}

  handleFieldKeyDown(event) {
    if (event.key === 'Enter' && !this.multiline) {
      event.preventDefault();
    }
  }

  handleFieldPaste(event) {
    event.preventDefault();

    let text = event.clipboardData.getData('text/plain');

    if (!this.multiline) {
      text = text.replace(/\n/g,' ');
    }

    document.execCommand('insertText', false, text);
  }
}

customElements.define('base-text-field', BaseTextField);
