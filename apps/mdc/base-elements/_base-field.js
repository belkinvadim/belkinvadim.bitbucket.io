const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      position: relative;
      box-sizing: border-box;
      display: block;
    }

    :host([hidden]) {
      display: none;
    }

    .label {
      position: absolute;
      top: 0;
      left: 16px;
      z-index: 2;
      display: block;
      max-width: calc(100% - 32px);
      font-size: 16px;
      font-weight: 400;
      letter-spacing: .15px;
      line-height: 1;
      text-transform: none;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
      color: var(--color-text-secondary-on-background);
      background-color: rgba(255, 255, 255, 0);
      transform: translateY(20px);
      transform-origin: left top;
      transition:
        top .15s var(--transition-standart-easing),
        color .15s var(--transition-standart-easing),
        background-color .15s var(--transition-standart-easing),
        transform .15s var(--transition-standart-easing);
      pointer-events: none;
    }

    .label:empty {
      display: none;
    }

    :host(:focus-within) .label {
      color: var(--color-primary);
      background-color: var(--color-background);
      box-shadow: 0 0 0 4px var(--color-background);
      transform: scale(.75) translateY(-50%);
    }

    ::slotted(input),
    ::slotted(textarea),
    ::slotted(select) {
      box-sizing: border-box;
      display: block;
      width: 100%;
      padding: 14px 16px;
      font-family: inherit;
      font-size: 16px;
      font-weight: 400;
      letter-spacing: .15px;
      line-height: 28px;
      text-transform: none;
      color: var(--color-text-primary-on-background);
      background-color: var(--color-background);
      border: 1px solid var(--color-divider-on-background);
      border-radius: 4px;
      box-shadow: none;
      transition:
        border-color .15s var(--transition-standart-easing),
        box-shadow .15s var(--transition-standart-easing);
      -webkit-appearance: none;
    }

    ::slotted(input:hover),
    ::slotted(textarea:hover),
    ::slotted(select:hover) {
      border-color: var(--color-text-primary-on-background);
    }

    ::slotted(input:focus),
    ::slotted(textarea:focus),
    ::slotted(select:focus) {
      border-color: var(--color-primary);
      box-shadow: inset 0 0 0 1px var(--color-primary);
      outline: none;
    }

    ::slotted(input),
    ::slotted(select:not([multiple])) {
      height: 56px;
    }

    ::slotted(textarea) {
      min-height: 56px;
      resize: vertical;
    }

    ::slotted(select:not([multiple])) {
      padding-right: 24px;
      background-image:
        linear-gradient(45deg, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, .54) 50%),
        linear-gradient(-45deg, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, .54) 50%);
      background-position:
        right 13px top 50%,
        right 8px top 50%;
      background-repeat: no-repeat;
      background-size: 5px 5px;
      appearance: none;
    }

    ::slotted(select[multiple]) {
      min-height: 56px;
    }
  </style>

  <div class="label"></div>

  <div class="control">
    <slot></slot>
  </div>
`;

class _baseField extends HTMLElement {
  static get observedAttributes() {
    return ['label'];
  }

  set label(value) {
    this.setAttribute('label', value);
  }

  get label() {
    return this.getAttribute('label');
  }

  constructor() {
    super();

    this.control = null;

    this.attachShadow({
      mode: 'open',
      delegatesFocus: true,
    });

    this.shadowRoot.appendChild(template.content.cloneNode(true));

    this.handleSlotChange = this.handleSlotChange.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }

  connectedCallback() {
    this.shadowRoot
      .querySelector('slot')
      .addEventListener('slotchange', this.handleSlotChange);
  }

  disconnectedCallback() {
    this.shadowRoot
      .querySelector('slot')
      .removeEventListener('slotchange', this.handleSlotChange);
  }

  attributeChangedCallback(attributeName, oldValue, newValue) {
    if (attributeName === 'label') {
      this.shadowRoot.querySelector('.label').textContent = newValue;
    }
  }

  handleSlotChange(event) {
    const nodes = event.target.assignedNodes();
    // console.log(nodes);

    /* let control = null;

    if (this.control !== null && !nodes.includes(this.control)) {
      this.control.removeEventListener('input', this.handleInput)
    }

    if (this.control === null) {
      this.control = nodes.reduce((control, node) => {
        if (control === null && ['INPUT', 'TEXTAREA', 'SELECT'].includes(node.nodeName)) {
          return node;
        }

        if (control !== node) {
          node.parentElement.removeChild(node);
        }

        return control;
      }, this.control);
    }

    console.log(this.control);

    Object.defineProperty(nodes[2], 'value', {
      set: function (value) {
        console.log(value);
      }
    }); */
  }

  handleInput(event) {
    console.log(event.target.value);
  }
}

customElements.define('base-field', _baseField);
