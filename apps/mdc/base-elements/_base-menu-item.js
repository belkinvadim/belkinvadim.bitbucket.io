const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      position: relative;
      box-sizing: border-box;
      overflow: hidden;
      display: block;
      width: 100%;
      min-height: 48px;
      padding: 12px 16px;
      font-size: 16px;
      font-weight: 400;
      line-height: 24px;
      letter-spacing: .5px;
      text-align: left;
      text-transform: none;
      user-select: none;
      cursor: pointer;
    }

    :host([hidden]) {
      display: none;
    }

    :host([disabled]) {
      color: var(--color-text-disabled-on-background);
      pointer-events: none;
      cursor: auto;
    }

    :host::before,
    :host::after {
      content: "";
      position: absolute;
      background-color: var(--color-on-background);
      opacity: 0;
      pointer-events: none;
    }

    :host::before {
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      transition:
        background-color 75ms var(--transition-standart-easing),
        opacity 75ms var(--transition-standart-easing);
    }

    :host(:hover)::before {
      opacity: .04;
    }

    :host(:focus)::before {
      opacity: .12;
    }

    :host::after {
      top: 50%;
      left: 50%;
      width: 110%;
      padding-bottom: 110%;
      border-radius: 50%;
      transform:
        translate(-50%, -50%)
        translateZ(-1px)
        scale(0);
      transform-origin: center center;
      transition:
        opacity .15s var(--transition-standart-easing),
        transform .15s var(--transition-standart-easing) .15s;
    }

    :host(:active)::after {
      opacity: .24;
      transform:
        translate(-50%, -50%)
        translateZ(-1px)
        scale(1);
      transition-duration: .225s;
      transition-delay: 0s;
    }
  </style>

  <slot></slot>
`;

class _baseMenuItem extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define('base-menu-item', _baseMenuItem);
