import BehaviorLink from '../behavior-elements/behavior-link.js';

const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      position: relative;
      box-sizing: border-box;
      display: block;
      height: 40px;
      margin: 8px;
      padding: 9px 8px 9px 64px;
      font-size: 14px;
      font-weight: 500;
      line-height: 22px;
      letter-spacing: .1px;
      text-align: left;
      text-decoration: none;
      color: var(--color-text-primary-on-background);
      background-color: transparent;
      border-radius: 3px;
      overflow: hidden;
      user-select: none;
      cursor: pointer;
      transition: color 75ms var(--transition-standart-easing);
    }

    :host([hidden]) {
      display: none;
    }

    :host(:focus) {
      outline: none;
    }

    :host([activated]) {
      color: var(--color-primary);
    }

    :host([disabled]) {
      color: var(--color-text-disabled-on-background);
      pointer-events: none;
      cursor: auto;
    }

    :host::before {
      content: "";
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      background-color: var(--color-on-background);
      opacity: 0;
      transition:
        background-color 75ms var(--transition-standart-easing),
        opacity 75ms var(--transition-standart-easing);
      pointer-events: none;
    }

    :host(:hover)::before {
      opacity: .04;
    }

    :host(:focus)::before {
      opacity: .12;
    }

    :host(:active)::before {
      opacity: .26;
    }

    :host([activated])::before {
      background-color: var(--color-primary);
      opacity: .12;
    }

    .icon {
      position: absolute;
      top: 50%;
      left: 8px;
      opacity: .6;
      transform: translateY(-50%);
    }

    :host([activated]) .icon {
      opacity: 1;
    }

    .content {
      display: block;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
    }
  </style>

  <div class="icon">
    <slot name="icon"></slot>
  </div>

  <div class="content">
    <slot></slot>
  </div>
`;

class BaseNavigationItem extends BehaviorLink {
  static get observedAttributes() {
    return ['activated'];
  }

  set activated(value) {
    const isActivated = Boolean(value);

    if (isActivated) {
      this.setAttribute('activated', '');
    } else {
      this.removeAttribute('activated');
    }
  }

  get activated() {
    return this.hasAttribute('activated');
  }

  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define('base-navigation-item', BaseNavigationItem);
