const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      position: relative;
      display: grid;
      grid-template-areas:
        "header"
        "body";
      grid-template-rows: max-content auto;
      height: 100%;
    }

    .header {
      grid-area: header;
      border-bottom: 1px solid var(--color-divider-on-background);
    }

    .body {
      position: relative;
      grid-area: body;
      overflow: auto;
    }
  </style>

  <div class="header">
    <slot name="header" />
  </div>

  <div class="body">
    <slot />
  </div>
`;

class BaseLayout extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define('base-layout', BaseLayout);
