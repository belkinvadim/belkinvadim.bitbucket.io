const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      position: relative;
      box-sizing: border-box;
      display: inline-block;
      width: 24px;
      height: 24px;
      overflow: hidden;
      vertical-align: top;
      opacity: 1;
    }

    :host([hidden]) {
      display: none;
    }

    .icon,
    .icon svg {
      display: block;
      width: 100%;
      height: 100%;
      overflow: hidden;
    }

    .icon svg {
      backface-visibility: hidden;
      fill: currentColor;
      stroke: currentColor;
      stroke-width: 0;
    }
  </style>

  <span class="icon"></span>
`;

class BaseIcon extends HTMLElement {
  static get observedAttributes() {
    return ['src'];
  }

  set src(value) {
    this.setAttribute('src', 'value');
  }

  get src() {
    return this.getAttribute('src');
  }

  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }

  attributeChangedCallback(attributeName, oldValue, newValue) {
    if (attributeName === 'src') {
      this.loadIcon(newValue);
    }
  }

  insertIcon(icon = '') {
    this.shadowRoot.querySelector('.icon').innerHTML = icon;
  }

  async loadIcon(path) {
    try {
      const response = await fetch(path);
      const contentType = response.headers.get('Content-Type');

      if (contentType !== 'image/svg+xml') {
        this.insertIcon();
        return;
      }

      const icon = await response.text();

      this.insertIcon(icon);
    } catch (error) {
      this.insertIcon();
    }
  }
}

customElements.define('base-icon', BaseIcon);
