import BehaviorMenu from '../behavior-elements/_behavior-menu.js';

const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      position: fixed;
      top: 0;
      left: 0;
      z-index: 80;
      box-sizing: border-box;
      display: block;
      padding: 8px 0;
      color: var(--color-on-surface);
      background-color: var(--color-surface);
      box-shadow: var(--shadow-elevation-8);
    }

    :host(:focus) {
      outline: none;
    }

    :host([hidden]) {
      display: none;
    }

    ::slotted(hr) {
      width: 100%;
      height: 0;
      margin: 4px 0;
      border: none;
      border-bottom: 1px solid var(--color-divider-on-background);
    }
  </style>

  <slot></slot>
`;

class _baseMenu extends BehaviorMenu {
  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define('base-menu', _baseMenu);
