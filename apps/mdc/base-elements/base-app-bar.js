const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      display: grid;
      height: 56px;
      padding-right: calc(var(--margin) - 12px);
      padding-left: calc(var(--margin) - 12px);
      grid-template-areas: "leading content actions";
      grid-template-columns: max-content auto max-content;
      align-items: center;
      color: var(--color-on-background);
      background-color: var(--color-background);
    }

    :host([hidden]) {
      display: none;
    }

    .leading {
      grid-area: leading;
    }

    .content {
      grid-area: content;
      padding-right: 20px;
      padding-left: 20px;
    }

    .actions {
      grid-area: actions;
    }

    @media (min-width: 720px) {
      :host {
        height: 64px;
      }
    }
  </style>

  <div class="leading">
    <slot name="leading" />
  </div>

  <div class="content">
    <slot />
  </div>

  <div class="actions">
    <slot name="actions" />
  </div>
`;

class BaseAppBar extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define('base-app-bar', BaseAppBar);
