import BehaviorButton from '../behavior-elements/behavior-button.js';

const template = document.createElement('template');

template.innerHTML = `
  <style>
    :host {
      position: relative;
      box-sizing: border-box;
      display: inline-block;
      width: 56px;
      height: 56px;
      padding: 16px;
      line-height: 24px;
      text-align: center;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
      color: var(--color-on-primary);
      background-color: var(--color-primary);
      border-radius: 28px;
      border: none;
      box-shadow: var(--shadow-elevation-6);
      user-select: none;
      cursor: pointer;
    }

    :host(:focus) {
      outline: none;
    }

    :host([hidden]) {
      display: none;
    }

    :host([disabled]) {
      color: var(--color-text-disabled-on-background);
      pointer-events: none;
      cursor: auto;
    }

    :host::after,
    :host::before {
      content: "";
      position: absolute;
      background-color: var(--color-on-primary);
      opacity: 0;
      pointer-events: none;
    }

    :host::before {
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      transition:
        background-color 75ms var(--transition-standart-easing),
        opacity 75ms var(--transition-standart-easing);
    }

    :host(:hover)::before {
      opacity: .08;
    }

    :host(:focus)::before {
      opacity: .24;
    }

    :host::after {
      top: 50%;
      left: 50%;
      width: 110%;
      padding-bottom: 110%;
      border-radius: 50%;
      transform:
        translate(-50%, -50%)
        translateZ(-1px)
        scale(0);
      transform-origin: center center;
      transition:
        opacity .15s var(--transition-standart-easing),
        transform .15s var(--transition-standart-easing) .15s;
    }

    :host(:active)::after {
      opacity: .24;
      transform:
        translate(-50%, -50%)
        translateZ(-1px)
        scale(1);
      transition-duration: .225s;
      transition-delay: 0s;
    }
  </style>

  <slot></slot>
`;

class BaseActionButton extends BehaviorButton {
  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define('base-action-button', BaseActionButton);
