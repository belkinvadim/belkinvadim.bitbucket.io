import { LitElement, html } from 'https://unpkg.com/lit-element?module';
import '../base/base-transaction-list-item.js';
import '../mdc/mdc-list.js';

class BaseTransactionList extends LitElement {
    static get properties() {
        return {
            transactions: { type: Array },
        };
    }

    constructor() {
        super();

        this.transactions = [];
    }

    render() {
        return html`
            <style>
                :host {
                    position: relative;
                    display: block;
                }

                .base-transaction-list {}

                .base-transaction-list__item {}
            </style>
    
            <mdc-list class="base-transaction-list">
                ${this.transactions.map(transaction => html`
                    <base-transaction-list-item
                        class="base-transaction-list__item"
                        .transaction="${transaction}"
                        @click="${this.handleClick}"
                    ></base-transaction-list-item>
                `)}
            </mdc-list>
        `;
    }

    handleClick() {
        console.log('handleClick');
    }
}

customElements.define('base-transaction-list', BaseTransactionList);
