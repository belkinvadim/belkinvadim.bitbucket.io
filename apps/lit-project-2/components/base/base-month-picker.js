import { LitElement, html } from 'https://unpkg.com/lit-element?module';

class BaseMonthPicker extends LitElement {
    static get properties() {
        return {
            max: {
                type: String,
            },

            min: {
                type: String,
            },

            month: {
                type: String,
            },
        };
    }

    get isDisabledNext() {
        return this.max <= this.month;
    }

    get isDisabledPrev() {
        return this.min >= this.month;
    }

    constructor() {
        super();

        const currentMonth = new Date().toISOString().slice(0, 7);
        this.max =  currentMonth;
        this.min =  currentMonth;
        this.month =  currentMonth;
    }

    get formattedMonth() {
        return new Date(this.month).toLocaleString('ru-RU', {
            month: 'long',
            year: 'numeric',
        });
    }

    render() {
        return html`
            <style>
                :host {
                    display: block;
                }

                .base-month-picker {
                    box-sizing: border-box;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    height: 56px;
                    padding-right: calc(var(--margin) - 12px);
                    padding-left: calc(var(--margin) - 12px);
                }
                
                .base-month-picker__date {
                    flex: 1 1 auto;
                    font-size: var(--font-size-body-1);
                    font-weight: 500;
                    letter-spacing: var(--letter-spacing-body-1);
                    line-height: var(--line-height-body-1);
                    text-transform: var(--text-transform-body-1);
                    text-align: center;
                }
                
                .base-month-picker__button {
                    flex: 0 0 auto;
                    width: 40px;
                    height: 40px;
                    background-color: #eee;
                }

                .base-month-picker__button[disabled] {
                    opacity: .2;
                }
            </style>

            <div class="base-month-picker">
                <button
                    class="base-month-picker__button"
                    type="button"
                    ?disabled="${this.isDisabledPrev}"
                    @click="${this.handleClickPrev}"
                ></button>

                <div class="base-month-picker__date">${this.formattedMonth}</div>

                <button
                    class="base-month-picker__button"
                    type="button"
                    ?disabled="${this.isDisabledNext}"
                    @click="${this.handleClickNext}"
                ></button>
            </div>
        `;
    }

    handleClickPrev() {
        const currentMonth = new Date(this.month);
        const prevMonth = currentMonth.setMonth(currentMonth.getMonth() - 1);
        const event = new CustomEvent('change', {
            detail: {
                month: new Date(prevMonth).toISOString().slice(0, 7),
            }
        });

        this.dispatchEvent(event);
    }

    handleClickNext() {
        const currentMonth = new Date(this.month);
        const nextMonth = currentMonth.setMonth(currentMonth.getMonth() + 1);
        const event = new CustomEvent('change', {
            detail: {
                month: new Date(nextMonth).toISOString().slice(0, 7),
            }
        });

        this.dispatchEvent(event);
    }
}

customElements.define('base-month-picker', BaseMonthPicker);
