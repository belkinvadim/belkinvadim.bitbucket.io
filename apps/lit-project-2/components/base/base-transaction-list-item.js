import { LitElement, html } from 'https://unpkg.com/lit-element?module';
import '../mdc/mdc-list-item.js';

class BaseTransactionListItem extends LitElement {
    static get properties() {
        return {
            transaction: {
                type: Object,
            },
        };
    }

    constructor() {
        super();

        this.transaction = {};
    }

    render() {
        return html`
            <style>
                :host {
                    position: relative;
                    display: block;
                }

                .base-transaction-list-item {
                    display: grid;
                    grid-column-gap: 16px;
                    grid-template-areas:
                      "icon category amount"
                      "icon account amount";
                    grid-template-columns: max-content auto max-content;
                    grid-template-rows: auto;
                    align-items: center;
                    padding: 12px 16px;
                }
                
                .base-transaction-list-item__category,
                .base-transaction-list-item__account,
                .base-transaction-list-item__amount {
                    text-overflow: ellipsis;
                    white-space: nowrap;
                    overflow: hidden;
                }
                
                .base-transaction-list-item__icon {
                    grid-area: icon;
                    box-sizing: border-box;
                    width: 40px;
                    height: 40px;
                    padding: 6px;
                    color: var(--color-on-primary);
                    background-color: var(--color-primary);
                    border-radius: 50%;
                }
                
                .base-transaction-list-item__category {
                    grid-area: category;
                }
                
                .base-transaction-list-item__account {
                    grid-area: account;
                    font-size: var(--font-size-body-2);
                    line-height: var(--line-height-body-2);
                    color: var(--color-text-secondary-on-background);
                }
                
                .base-transaction-list-item__amount {
                    grid-area: amount;
                }
            </style>
    
            <mdc-list-item class="base-transaction-list-item">
                <span class="base-transaction-list-item__icon"></span>
                <span class="base-transaction-list-item__category">${this.transaction.category.name}</span>
                <span class="base-transaction-list-item__account">${this.transaction.account.name}</span>
                <span class="base-transaction-list-item__amount">${this.transaction.amount}</span>
            </mdc-list-item>
        `;
    }
}

customElements.define('base-transaction-list-item', BaseTransactionListItem);
