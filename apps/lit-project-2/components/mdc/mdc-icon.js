import { LitElement, html } from 'https://unpkg.com/lit-element?module';

class MDCIcon extends LitElement {
    static get properties() {
        return {
            icon: {
                type: String,
            },
        };
    }

    constructor() {
        super();

        this.icon = 'undefined';
    }

    render() {
        return html`
            <style>
                :host {
                    position: relative;
                    box-sizing: border-box;
                    display: inline-block;
                    width: 24px;
                    height: 24px;
                    overflow: hidden;
                    vertical-align: top;
                    opacity: 1;
                }

                .mdc-icon {
                    display: block;
                    width: 100%;
                    height: 100%;
                    overflow: hidden;
                    backface-visibility: hidden;
                    fill: currentColor;
                    stroke: currentColor;
                    stroke-width: 0;
                }
            </style>

            <svg
              class="mdc-icon"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
            >
              <use xlink:href="../../icons/${this.icon}.svg"></use>
            </svg>
        `;
    }
}

customElements.define('mdc-icon', MDCIcon);
