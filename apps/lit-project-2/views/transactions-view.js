import { LitElement, html } from 'https://unpkg.com/lit-element?module';
import database from '../database.js';
import '../components/base/base-layout.js';
import '../components/base/base-month-picker.js';
import '../components/base/base-transaction-list.js';
import '../components/mdc/mdc-toolbar.js';

const currentMonthDate = new Date(new Date().toISOString().slice(0, 7)).getTime();

class TransactionsView extends LitElement {
    static get properties() {
        return {
            accounts: {
                type: Array,
            },

            categories: {
                type: Array,
            },

            month: {
                type: String,
            },

            transactions: {
                type: Array,
            },
        };
    }

    get maxMonth() {
        const maxMonth = this.transactions
            .reduce((date, transaction) => Math.max(transaction.date, date), currentMonthDate);

        return new Date(maxMonth).toISOString().slice(0, 7);
    }

    get minMonth() {
        const minMonth = this.transactions
            .reduce((date, transaction) => Math.min(transaction.date, date), currentMonthDate);

        return new Date(minMonth).toISOString().slice(0, 7);
    }

    get formattedTransaction() {
        return this.transactions
            .filter((transaction) => new Date(transaction.date).toISOString().slice(0, 7) === this.month)
            .map((transaction) => {
                const { accountId, categoryId, ...others } = transaction;
                const transactionAccount = this.accounts.find(account => account.id === accountId);
                const transactionCategory = this.categories.find(category => category.id === categoryId);
                const amount = transaction.amount.toLocaleString('ru-RU', {
                    style: 'currency',
                    currency: transactionAccount.currency,
                });

                return {
                    ...others,
                    amount,
                    account: transactionAccount,
                    category: transactionCategory,
                };
            });
    }

    constructor() {
        super();

        const { accounts, categories, transactions } = database;

        this.accounts = accounts;
        this.categories = categories;
        this.transactions = transactions;
        this.month = new Date().toISOString().slice(0, 7);
    }

    render() {
        return html`
            <style>
                :host {
                    display: block;
                    height: 100%;
                }
            </style>

            <base-layout>
                <mdc-toolbar slot="header"></mdc-toolbar>

                <base-month-picker
                    .max="${this.maxMonth}"
                    .min="${this.minMonth}"
                    .month="${this.month}"
                    @change="${this.handleChangeMonth}"
                ></base-month-picker>

                <base-transaction-list .transactions="${this.formattedTransaction}"></base-transaction-list>
            </base-layout>
        `;
    }

    handleChangeMonth(event) {
        this.month = event.detail.month;
    }
}

customElements.define('transactions-view', TransactionsView);
