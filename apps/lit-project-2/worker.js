self.addEventListener('message', (event) => {
    if (event.data.cmd) {
        const {
            accounts,
            categories,
            month,
            transactions
        } = event.data;
        console.log(month);

        const formattedTransactions = transactions
            .filter((transaction) => new Date(transaction.date).toISOString().slice(0, 7) === month)
            .map((transaction) => {
                const { accountId, categoryId, ...others } = transaction;
                const transactionAccount = accounts.find(account => account.id === accountId);
                const transactionCategory = categories.find(category => category.id === categoryId);
                const amount = transaction.amount.toLocaleString('ru-RU', {
                    style: 'currency',
                    currency: transactionAccount.currency,
                });

                return {
                    ...others,
                    amount,
                    account: transactionAccount,
                    category: transactionCategory,
                };
            });

        self.postMessage(formattedTransactions);
    }
});
