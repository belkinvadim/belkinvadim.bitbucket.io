import { LitElement, html } from 'https://unpkg.com/lit-element?module';
import './views/transactions-view.js';

class App extends LitElement {
    static get properties() {
        return {
            view: {
                type: String,
            },
        };
    }

    constructor() {
        super();

        this.currentView = 'transactions';
    }

    get view() {
        switch (this.currentView) {
            case 'transactions':
                return html`<transactions-view></transactions-view>`;
            default:
                return html`Empty`
        }
    }

    render() {
        return html`
            <style>
                :host {
                    display: block;
                    height: 100%;
                }
            </style>

            ${this.view}
        `;
    }
}

customElements.define('lit-app', App);
