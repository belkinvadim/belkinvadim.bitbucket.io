import { CURRENCY_SIGNS } from './const.js';

export const getFormattedPrice = (price, currency) => {
    const formattedPrice = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    const currencySign = CURRENCY_SIGNS[currency];

    return `${formattedPrice} ${currencySign}`;
};
