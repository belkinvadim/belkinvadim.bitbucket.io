import { LitElement, html } from 'https://unpkg.com/lit-element?module';

class BaseLayout extends LitElement {
    render() {
        return html`
            <style>
                :host {
                    position: relative;
                    display: block;
                    height: 100%;
                }

                .base-layout {
                    display: grid;
                    grid-template-areas:
                      "header"
                      "body";
                    grid-template-columns: 100%;
                    grid-template-rows: max-content auto;
                    height: 100%;
                }

                .base-layout__header {
                    position: sticky;
                    bottom: 0;
                    z-index: 80;
                    grid-area: header;
                    color: var(--color-on-background);
                    background-color: var(--color-background);
                    box-shadow: 0 1px 0 var(--color-divider-on-background);
                    transition: box-shadow 280ms var(--transition-standart-easing);
                }

                .base-layout__body {
                    grid-area: body;
                    overflow: auto;
                }
            </style>
    
            <div class="base-layout">
                <div class="base-layout__header">
                    <slot name="header"></slot>
                </div>
                <div class="base-layout__body">
                    <slot></slot>
                </div>
            </div>
        `;
    }
}

customElements.define('base-layout', BaseLayout);
