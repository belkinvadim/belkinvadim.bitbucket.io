import { LitElement, html } from 'https://unpkg.com/lit-element?module';
import '../mdc/mdc-toolbar.js';

class BaseAppBar extends LitElement {
    static get properties() {
        return {
            heading: {
                type: String,
            },
        };
    }

    constructor() {
        super();

        this.heading = '';
    }

    render() {
        return html`
            <style>
                :host {
                    position: relative;
                    display: block;
                }

                .base-app-bar {
                    grid-template-areas: "leading heading actions";
                    grid-template-columns: max-content auto max-content;
                }

                .base-app-bar_leading {
                    grid-area: leading;
                }

                .base-app-bar__heading {
                    grid-area: heading;
                    display: block;
                    margin: 0;
                    padding: 0 12px;
                    font-size: var(--font-size-headline-6);
                    font-weight: var(--font-weight-headline-6);
                    letter-spacing: var(--letter-spacing-headline-6);
                    line-height: var(--line-height-headline-6);
                    text-transform: var(--text-transform-headline-6);
                    text-align: center;
                    text-overflow: ellipsis;
                    white-space: nowrap;
                    overflow: hidden;
                }

                .base-app-bar_actions {
                    display: grid;
                    grid-area: actions;
                    grid-auto-flow: column;
                    grid-auto-columns: max-content;
                    align-items: center;
                }
            </style>
    
            <mdc-toolbar class="base-app-bar">
                <div class="base-app-bar__leading">
                    <slot name="leading"></slot>
                </div>

                <h1 class="base-app-bar__heading">
                    ${this.heading}
                </h1>

                <div class="base-app-bar__actions">
                    <slot name="actions"></slot>
                </div>
            </mdc-toolbar>
        `;
    }
}

customElements.define('base-app-bar', BaseAppBar);
