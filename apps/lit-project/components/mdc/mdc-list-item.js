import { LitElement, html } from 'https://unpkg.com/lit-element?module';

class MDCListItem extends LitElement {
    render() {
        return html`
            <style>
                :host {
                    position: relative;
                    display: grid;
                    align-items: center;
                    min-height: 48px;
                    padding: 12px 16px;
                    font-size: var(--font-size-body-1);
                    line-height: var(--line-height-body-1);
                    user-select: none;
                    cursor: pointer;
                }

                :host::before {
                    content: "";
                    position: absolute;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    left: 0;
                    background-color: #000;
                    opacity: 0;
                    transition:
                        background-color 75ms var(--transition-standart-easing),
                        opacity 75ms var(--transition-standart-easing);
                    pointer-events: none;
                }
                
                :host(:hover)::before {
                    opacity: .04;
                }
                
                :host(:focus)::before {
                    opacity: .12;
                }
                
                :host(:active)::before {
                    opacity: .26;
                }
            </style>

            <slot></slot>
        `;
    }
}

customElements.define('mdc-list-item', MDCListItem);
