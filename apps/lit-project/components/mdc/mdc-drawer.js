import { LitElement, html } from 'https://unpkg.com/lit-element?module';

class MDCDrawer extends LitElement {
    render() {
        return html`
            <style>
                :host {}

                :host(:not([open])) {
                    pointer-events: none;
                }
    
                .mdc-drawer {
                    position: fixed;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    z-index: 160;
                }

                .mdc-drawer__backdrop {
                    position: fixed;
                    top: 0;
                    left: 0;
                    z-index: 1;
                    width: 100%;
                    height: 100%;
                    background-color: #000;
                    opacity: 0;
                    cursor: pointer;
                    transition: opacity .25s var(--transition-standart-easing);
                    will-change: opacity;
                }
                
                :host([open]) .mdc-drawer__backdrop {
                    opacity: .3;
                }
                
                .mdc-drawer__container {
                    position: relative;
                    z-index: 2;
                    width: 100%;
                    max-width: 280px;
                    height: 100%;
                    overflow: auto;
                    background-color: var(--color-surface);
                    box-shadow: none;
                    transform: translateX(-100%);
                    transition:
                      box-shadow .25s var(--transition-standart-easing),
                      transform .25s var(--transition-standart-easing);
                    will-change: transform;
                }
                
                :host([open]) .mdc-drawer__container {
                    transform: translateX(0%);
                    box-shadow: var(--shadow-elevation-16);
                }
            </style>

            <div class="mdc-drawer">
                <div
                    class="mdc-drawer__backdrop"
                    @click="${this.handleClickBackdrop}"
                ></div>

                <div class="mdc-drawer__container">
                    <slot></slot>
                </div>
            </div>
        `;
    }

    handleClickBackdrop() {
        const event = new CustomEvent('close');

        this.dispatchEvent(event);
    }
}

customElements.define('mdc-drawer', MDCDrawer);
