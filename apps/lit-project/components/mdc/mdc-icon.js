import { LitElement, html } from 'https://unpkg.com/lit-element?module';

class MDCIcon extends LitElement {
    static get properties() {
        return {
            icon: {
                type: String,
            },
        };
    }

    constructor() {
        super();

        this.icon = '';
    }

    render() {
        return html`
            <style>
                :host {
                    position: relative;
                    box-sizing: border-box;
                    display: grid;
                    align-content: center;
                    justify-content: center;
                    width: 1em;
                    height: 1em;
                    font-family: 'Material Icons', serif;
                    font-size: 24px;
                    line-height: 1;
                    overflow: hidden;
                    vertical-align: top;
                    opacity: 1;
                }

                :host::before {
                    content: attr(icon);
                }
            </style>
        `;
    }
}

customElements.define('mdc-icon', MDCIcon);
