import { LitElement, html } from 'https://unpkg.com/lit-element?module';

class MDCList extends LitElement {
    render() {
        return html`
            <style>
                :host {
                    display: block;
                    padding-top: 8px;
                    padding-bottom: 8px;
                }
            </style>

            <slot></slot>
        `;
    }
}

customElements.define('mdc-list', MDCList);
