import { LitElement, html } from 'https://unpkg.com/lit-element?module';
import './mdc-icon.js'

class MDCIconButton extends LitElement {
    static get properties() {
        return {
            icon: {
                type: String,
            },
        };
    }

    constructor() {
        super();

        this.icon = '';
    }

    render() {
        return html`
            <style>
                :host {
                    box-sizing: border-box;
                    position: relative;
                    display: inline-flex;
                    margin: 0;
                    align-items: center;
                    justify-content: center;
                    width: auto;
                    height: auto;
                    padding: 12px;
                    color: rgba(0, 0, 0, .6);
                    background-color: transparent;
                    border: none;
                    box-shadow: none;
                    user-select: none;
                    outline: none;
                    cursor: pointer;
                }

                :host([disabled]) {
                    color: rgba(0, 0, 0, .37);
                    pointer-events: none;
                    cursor: auto;
                }

                :host::before {}

                :host::before {
                    content: "";
                    position: absolute;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    left: 0;
                    background-color: #000;
                    border-radius: 50%;
                    opacity: 0;
                    transition:
                      background-color 75ms var(--transition-standart-easing),
                      opacity 75ms var(--transition-standart-easing);
                    pointer-events: none;
                }
                
                :host(:hover)::before {
                    opacity: .04;
                }
                
                :host(:focus)::before {
                    opacity: .12;
                }
                
                :host(:active)::before {
                    opacity: .26;
                }
            </style>

            <mdc-icon icon="${this.icon}"></mdc-icon>
        `;
    }
}

customElements.define('mdc-icon-button', MDCIconButton);
