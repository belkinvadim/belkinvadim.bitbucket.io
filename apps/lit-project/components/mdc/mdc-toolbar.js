import { LitElement, html } from 'https://unpkg.com/lit-element?module';

class MDCToolbar extends LitElement {
    render() {
        return html`
            <style>
                :host {
                    display: grid;
                    grid-auto-flow: column;
                    grid-auto-columns: max-content;
                    height: 56px;
                    padding-right: calc(var(--margin) - 12px);
                    padding-left: calc(var(--margin) - 12px);
                    align-items: center;
                    color: var(--color-on-background);
                    background-color: var(--color-background);
                }
                
                @media (min-width: 720px) {
                    :host {
                      height: 64px;
                    }
                }
            </style>
    
            <slot></slot>
        `;
    }
}

customElements.define('mdc-toolbar', MDCToolbar);
