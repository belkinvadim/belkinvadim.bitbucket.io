import { LitElement, html } from 'https://unpkg.com/lit-element?module';
import database from '../database.js';
import '../components/base/base-app-bar.js';
import '../components/base/base-layout.js';
import '../components/base/base-month-picker.js';
import '../components/base/base-transaction-list.js';
import '../components/mdc/mdc-drawer.js';
import '../components/mdc/mdc-icon-button.js';

class TransactionsView extends LitElement {
    static get properties() {
        return {
            accounts: {
                type: Array,
            },

            categories: {
                type: Array,
            },

            isOpenDrawer: {
                type: Boolean,
            },

            month: {
                type: String,
            },

            transactions: {
                type: Array,
            },
        };
    }

    get formattedTransaction() {
        return this.transactions
            .filter((transaction) => new Date(transaction.date).toISOString().slice(0, 7) === this.month)
            .map((transaction) => {
                const { accountId, categoryId, ...others } = transaction;
                const transactionAccount = this.accounts.find(account => account.id === accountId);
                const transactionCategory = this.categories.find(category => category.id === categoryId);

                return {
                    ...others,
                    account: transactionAccount,
                    category: transactionCategory,
                };
            });
    }

    constructor() {
        super();

        const { accounts, categories, transactions } = database;
        const currentMonthDate = new Date(new Date().toISOString().slice(0, 7)).getTime();
        const minMonthDate = transactions
            .reduce((date, transaction) => Math.min(transaction.date, date), currentMonthDate);
        const maxMonthDate = transactions
            .reduce((date, transaction) => Math.max(transaction.date, date), currentMonthDate);

        this.accounts = accounts;
        this.categories = categories;
        this.transactions = transactions;
        this.month = new Date().toISOString().slice(0, 7);
        this.minMonth = new Date(minMonthDate).toISOString().slice(0, 7);
        this.maxMonth = new Date(maxMonthDate).toISOString().slice(0, 7);
        this.isOpenDrawer = false;
    }

    render() {
        return html`
            <style>
                :host {
                    display: block;
                    height: 100%;
                }
            </style>

            <base-layout>
                <base-app-bar
                    slot="header"
                    heading="Транзакции"
                >
                    <mdc-icon-button
                        slot="leading"
                        icon="menu"
                        @click="${this.handleClickMenuButton}"
                    ></mdc-icon-button>

                    <mdc-icon-button
                        slot="actions"
                        icon="filter_list"
                    ></mdc-icon-button>
                </base-app-bar>

                <mdc-drawer
                    ?open="${this.isOpenDrawer}"
                    @close="${this.handleClickDrawerBackdrop}"
                ></mdc-drawer>

                <base-month-picker
                    max="${this.maxMonth}"
                    min="${this.minMonth}"
                    month="${this.month}"
                    @change="${this.handleChangeMonth}"
                ></base-month-picker>

                <base-transaction-list
                    .transactions="${this.formattedTransaction}"
                ></base-transaction-list>
            </base-layout>
        `;
    }

    handleChangeMonth(event) {
        this.month = event.detail.month;
    }

    handleClickMenuButton() {
        this.isOpenDrawer = true;
    }

    handleClickDrawerBackdrop() {
        this.isOpenDrawer = false;
    }
}

customElements.define('transactions-view', TransactionsView);
