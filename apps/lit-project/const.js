export const CURRENCY_SIGNS = {
    EUR: '€',
    RUB: '₽',
    USD: '$',
};
