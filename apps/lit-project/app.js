import { LitElement, html } from 'https://unpkg.com/lit-element?module';
import './views/transactions-view.js';

class App extends LitElement {
    render() {
        return html`
            <style>
                :host {
                    display: block;
                    height: 100%;
                }
            </style>

            <transactions-view></transactions-view>
        `;
    }
}

customElements.define('lit-app', App);
