class MDSubtitle extends HTMLElement {
    static get is() {
        return 'md-subtitle';
    }

    static get template() {
        return `
            <link rel=stylesheet href="./components/md-subtitle/md-subtitle.css">
            <div class="md-subtitle" role="heading">
                <slot></slot>
            </div>
        `;
    }

    static get observedAttributes() {
        return ['data-type'];
    }

    constructor() {
        super();

        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = this.constructor.template;
        this.el = this.shadowRoot.querySelector('.md-subtitle');
    }

    attributeChangedCallback(attributeName, oldValue, newValue) {
        if (attributeName === 'data-type') {
            if (oldValue) {
                this.el.classList.remove(`md-subtitle--type-${oldValue}`);
            }

            if (newValue) {
                this.el.classList.add(`md-subtitle--type-${newValue}`);
            }
        }
    }
}

window.customElements.define(MDSubtitle.is, MDSubtitle);