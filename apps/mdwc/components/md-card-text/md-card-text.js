class MDCardText extends HTMLElement {
    static get is() {
        return 'md-card-text';
    }

    static get template() {
        return `
            <link rel=stylesheet href="./components/md-card-text/md-card-text.css">
            <div class="md-card-text">
                <slot></slot>
            </div>
        `;
    }

    constructor() {
        super();

        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = this.constructor.template;
    }
}

window.customElements.define(MDCardText.is, MDCardText);