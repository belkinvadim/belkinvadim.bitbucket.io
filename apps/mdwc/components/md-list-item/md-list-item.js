class MDListItem extends HTMLElement {
    static get is() {
        return 'md-list-item';
    }

    static get template() {
        return `
            <link rel=stylesheet href="./components/md-list-item/md-list-item.css">
            <div class="md-list-item" tabindex="0">
                <div class="md-list-item__content">
                    <slot></slot>
                </div>
            </div>
        `;
    }

    static get observedAttributes() {
        return ['data-type'];
    }

    constructor() {
        super();

        this.attachShadow({
            delegatesFocus: true,
            mode: 'open',
        });

        this.shadowRoot.innerHTML = this.constructor.template;
        this.el = this.shadowRoot.querySelector('.md-list-item');
    }

    attributeChangedCallback(attributeName, oldValue, newValue) {
        if (attributeName === 'data-type') {
            if (oldValue) {
                this.el.classList.remove(`md-list-item--type-${oldValue}`);
            }

            if (newValue) {
                this.el.classList.add(`md-list-item--type-${newValue}`);
            }
        }
    }
}

window.customElements.define(MDListItem.is, MDListItem);