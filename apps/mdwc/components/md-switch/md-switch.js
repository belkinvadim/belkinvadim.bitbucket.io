import codes from '../../constants/codes.js';

class MDSwitch extends HTMLElement {
    static get is() {
        return 'md-switch';
    }

    static get template() {
        return `
            <link rel=stylesheet href="./components/md-switch/md-switch.css">
            <div class="md-switch" tabindex="0">
                <div class="md-switch__bar"></div>
                <div class="md-switch__button"></div>
            </div>
        `;
    }

    static get observedAttributes() {
        return ['checked', 'disabled', 'pressed'];
    }

    set checked(value) {
        const isChecked = Boolean(value);

        if (isChecked) {
            this.setAttribute('checked', '');
        }
        else {
            this.removeAttribute('checked');
        }
    }

    get checked() {
        return this.hasAttribute('checked');
    }

    set disabled(value) {
        const isDisabled = Boolean(value);

        if (isDisabled) {
            this.setAttribute('disabled', '');
        }
        else {
            this.removeAttribute('disabled');
        }
    }

    get disabled() {
        return this.hasAttribute('disabled');
    }

    set pressed(value) {
        const isPressed = Boolean(value);

        if (isPressed) {
            this.setAttribute('pressed', '');
        }
        else {
            this.removeAttribute('pressed');
        }
    }

    get pressed() {
        return this.hasAttribute('pressed');
    }

    constructor() {
        super();

        this.attachShadow({ mode: 'open' });

        this.shadowRoot.innerHTML = this.constructor.template;
        this.el = this.shadowRoot.querySelector('.md-switch');

        this.handleClick = this.handleClick.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleKeyUp = this.handleKeyUp.bind(this);
    }

    connectedCallback() {
        this.addEventListener('click', this.handleClick);
        this.addEventListener('keydown', this.handleKeyDown);
        this.addEventListener('keyup', this.handleKeyUp);
    }

    disconnectedCallback() {
        this.removeEventListener('click', this.handleClick);
        this.removeEventListener('keydown', this.handleKeyDown);
        this.removeEventListener('keyup', this.handleKeyUp);
    }

    attributeChangedCallback(attributeName, oldValue, newValue) {
        if (attributeName === 'checked') {
            const isChecked = this.hasAttribute('checked') && (newValue === true || newValue === '');

            this.el.classList.toggle(`is-checked`, isChecked);
        }

        if (attributeName === 'disabled') {
            const isDisabled = this.hasAttribute('disabled') && (newValue === true || newValue === '');

            this.el.classList.toggle(`is-disabled`, isDisabled);
            this.el.setAttribute('tabindex', isDisabled ? -1 : 0);
        }

        if (attributeName === 'pressed') {
            const isPressed = this.hasAttribute('pressed') && (newValue === true || newValue === '');

            this.el.classList.toggle(`is-pressed`, isPressed);
        }
    }

    change() {
        this.checked = !this.checked;

        this.dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true
        }));
    }

    handleClick() {
        if (this.disabled) {
            return;
        }

        this.change();
    }

    handleKeyDown(event) {
        const { code } = event;
        const { CODE_SPACE } = codes;

        if (code === CODE_SPACE && !this.disabled) {
            // Prevent the default action to stop scrolling when space is pressed
            event.preventDefault();

            this.pressed = true;
        }
    }

    handleKeyUp(event) {
        const { code } = event;
        const { CODE_SPACE } = codes;

        if (code === CODE_SPACE && !this.disabled) {
            // Prevent the default action to stop scrolling when space is pressed
            event.preventDefault();

            this.pressed = false;

            this.change();
        }
    }
}

window.customElements.define(MDSwitch.is, MDSwitch);