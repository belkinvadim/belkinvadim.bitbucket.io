import codes from '../../constants/codes.js';

class MDButton extends HTMLElement {
    static get is() {
        return 'md-button';
    }

    static get template() {
        return `
            <link rel=stylesheet href="./components/md-button/md-button.css">
            <div class="md-button" tabindex="0" role="button">
                <slot></slot>
            </div>
        `;
    }

    static get observedAttributes() {
        return ['data-theme', 'data-type', 'disabled', 'pressed'];
    }

    set disabled(value) {
        const isDisabled = Boolean(value);

        if (isDisabled) {
            this.setAttribute('disabled', '');
        }
        else {
            this.removeAttribute('disabled');
        }
    }

    get disabled() {
        return this.hasAttribute('disabled');
    }

    set pressed(value) {
        const isPressed = Boolean(value);

        if (isPressed) {
            this.setAttribute('pressed', '');
        }
        else {
            this.removeAttribute('pressed');
        }
    }

    get pressed() {
        return this.hasAttribute('pressed');
    }

    constructor() {
        super();

        this.attachShadow({
            delegatesFocus: true,
            mode: 'open',
        });

        this.shadowRoot.innerHTML = this.constructor.template;
        this.el = this.shadowRoot.querySelector('.md-button');

        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleKeyUp = this.handleKeyUp.bind(this);
    }

    connectedCallback() {
        this.addEventListener('keydown', this.handleKeyDown);
        this.addEventListener('keyup', this.handleKeyUp);
    }

    disconnectedCallback() {
        this.removeEventListener('keydown', this.handleKeyDown);
        this.removeEventListener('keyup', this.handleKeyUp);
    }

    attributeChangedCallback(attributeName, oldValue, newValue) {
        const hasAttribute = this.hasAttribute(attributeName);

        if (attributeName === 'data-theme') {
            if (oldValue) {
                this.el.classList.remove(`md-button--theme-${oldValue}`);
            }

            if (newValue) {
                this.el.classList.add(`md-button--theme-${newValue}`);
            }
        } else if(attributeName === 'data-type') {
            if (oldValue) {
                this.el.classList.remove(`md-button--type-${oldValue}`);
            }

            if (newValue) {
                this.el.classList.add(`md-button--type-${newValue}`);
            }
        } else {
            this.el.classList.toggle(`is-${attributeName}`, hasAttribute);
        }

        if (attributeName === 'disabled') {
            this.el.setAttribute('tabindex', hasAttribute ? -1 : 0);
        }
    }

    handleKeyDown(event) {
        const { code } = event;
        const { CODE_SPACE, CODE_ENTER } = codes;

        if ((code === CODE_SPACE || code === CODE_ENTER) && !this.disabled) {
            // Prevent the default action to stop scrolling when space is pressed
            event.preventDefault();

            this.pressed = true;
        }
    }

    handleKeyUp(event) {
        const { code } = event;
        const { CODE_SPACE, CODE_ENTER } = codes;

        if ((code === CODE_SPACE || code === CODE_ENTER) && !this.disabled) {
            // Prevent the default action to stop scrolling when space is pressed
            event.preventDefault();

            this.pressed = false;

            this.dispatchEvent(new Event('click', {
                bubbles: true,
                cancelable: true
            }));
        }
    }
}

window.customElements.define(MDButton.is, MDButton);