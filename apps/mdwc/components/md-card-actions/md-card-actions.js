class MDCardActions extends HTMLElement {
    static get is() {
        return 'md-card-actions';
    }

    static get template() {
        return `
            <link rel=stylesheet href="./components/md-card-actions/md-card-actions.css">
            <div class="md-card-actions">
                <slot></slot>
            </div>
        `;
    }

    constructor() {
        super();

        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = this.constructor.template;
    }
}

window.customElements.define(MDCardActions.is, MDCardActions);