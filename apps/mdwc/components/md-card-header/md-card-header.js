class MDCardHeader extends HTMLElement {
    static get is() {
        return 'md-card-header';
    }

    static get template() {
        return `
            <link rel=stylesheet href="./components/md-card-header/md-card-header.css">
            <div class="md-card-header">
                <slot></slot>
            </div>
        `;
    }

    constructor() {
        super();

        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = this.constructor.template;
    }
}

window.customElements.define(MDCardHeader.is, MDCardHeader);