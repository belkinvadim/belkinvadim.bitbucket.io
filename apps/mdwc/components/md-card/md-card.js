class MDCard extends HTMLElement {
    static get is() {
        return 'md-card';
    }

    static get template() {
        return `
            <link rel=stylesheet href="./components/md-card/md-card.css">
            <div class="md-card">
                <slot></slot>
            </div>
        `;
    }

    static get observedAttributes() {
        return ['type'];
    }

    constructor() {
        super();

        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = this.constructor.template;
    }
}

window.customElements.define(MDCard.is, MDCard);