class MDHeadline extends HTMLElement {
    static get is() {
        return 'md-headline';
    }

    static get template() {
        return `
            <link rel=stylesheet href="./components/md-headline/md-headline.css">
            <div class="md-headline" role="heading">
                <slot></slot>
            </div>
        `;
    }

    static get observedAttributes() {
        return ['data-type'];
    }

    constructor() {
        super();

        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = this.constructor.template;
        this.el = this.shadowRoot.querySelector('.md-headline');
    }

    attributeChangedCallback(attributeName, oldValue, newValue) {
        if (attributeName === 'data-type') {
            if (oldValue) {
                this.el.classList.remove(`md-headline--type-${oldValue}`);
            }

            if (newValue) {
                this.el.classList.add(`md-headline--type-${newValue}`);
            }
        }
    }
}

window.customElements.define(MDHeadline.is, MDHeadline);