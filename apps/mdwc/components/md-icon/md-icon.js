class MDIcon extends HTMLElement {
    static get is() {
        return 'md-icon';
    }

    static get template() {
        return `
            <link rel=stylesheet href="./components/md-icon/md-icon.css">
            <svg viewBox="0 0 24 24">
                <use xlink:href=""></use>
            </svg>
        `;
    }

    static get observedAttributes() {
        return ['icon'];
    }

    set icon(value) {
        this.setAttribute('icon', value);
    }

    get icon() {
        return this.getAttribute('icon');
    }

    constructor() {
        super();

        this.attachShadow({mode: 'open'});
        this.shadowRoot.innerHTML = this.constructor.template;
    }

    attributeChangedCallback(attributeName, oldValue, newValue, namespace) {
        this.shadowRoot.querySelector('use').setAttribute('xlink:href', `./components/md-icon/icons/${newValue}.svg`);
    }
}

window.customElements.define(MDIcon.is, MDIcon);