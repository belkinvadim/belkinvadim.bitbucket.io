import codes from '../../constants/codes.js';
import '../md-icon/md-icon.js';

class MDIconButton extends HTMLElement {
    static get is() {
        return 'md-icon-button';
    }

    static get template() {
        return `
            <link rel=stylesheet href="./components/md-icon-button/md-icon-button.css">
            <md-icon></md-icon>
        `;
    }

    static get observedAttributes() {
        return ['pressed', 'disabled', 'icon'];
    }

    set pressed(value) {
        const isPressed = Boolean(value);

        if (isPressed) {
            this.setAttribute('pressed', '');
        }
        else {
            this.removeAttribute('pressed');
        }
    }

    get pressed() {
        return this.hasAttribute('pressed');
    }

    set disabled(value) {
        const isDisabled = Boolean(value);

        if (isDisabled) {
            this.setAttribute('disabled', '');
        }
        else {
            this.removeAttribute('disabled');
        }
    }

    get disabled() {
        return this.hasAttribute('disabled');
    }

    constructor() {
        super();

        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleKeyUp = this.handleKeyUp.bind(this);

        this.attachShadow({mode: 'open'});
        this.shadowRoot.innerHTML = this.constructor.template;

        this.addEventListener('keydown', this.handleKeyDown);
        this.addEventListener('keyup', this.handleKeyUp);
    }

    attributeChangedCallback(attributeName, oldValue, newValue, namespace) {
        if (attributeName === 'icon') {
            this.shadowRoot.querySelector('md-icon').icon = newValue;
        }
    }

    handleKeyDown(event) {
        const { code } = event;
        const { CODE_SPACE, CODE_ENTER } = codes;

        if ((code === CODE_SPACE || code === CODE_ENTER) && !this.disabled) {
            // Prevent the default action to stop scrolling when space is pressed
            event.preventDefault();

            this.pressed = true;
        }
    }

    handleKeyUp(event) {
        const { code } = event;
        const { CODE_SPACE, CODE_ENTER } = codes;

        if ((code === CODE_SPACE || code === CODE_ENTER) && !this.disabled) {
            // Prevent the default action to stop scrolling when space is pressed
            event.preventDefault();

            this.pressed = false;

            this.dispatchEvent(new Event('click', {
                bubbles: true,
                cancelable: true
            }));
        }
    }
}

window.customElements.define(MDIconButton.is, MDIconButton);